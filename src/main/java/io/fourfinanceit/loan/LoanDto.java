package io.fourfinanceit.loan;

import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

class LoanDto {

	@Getter
	private Integer id;
	@Getter
	private BigDecimal amount;
	@Getter
	private LocalDateTime returnDate;

	LoanDto() {
	}

	LoanDto(Integer id, BigDecimal amount, LocalDateTime returnDate) {
		this.id = id;
		this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);
		this.returnDate = returnDate;
	}

}
