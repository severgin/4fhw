package io.fourfinanceit.loan;

import io.fourfinanceit.geolocation.IPAddressRetrievalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;

@RestController
@RequestMapping(path = "/loan", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
class LoanResource {

	private final LoanService loanService;
	private final IPAddressRetrievalService ipAddressRetrievalService;

	@Autowired
	LoanResource(LoanService loanService, IPAddressRetrievalService ipAddressRetrievalService) {
		this.loanService = loanService;
		this.ipAddressRetrievalService = ipAddressRetrievalService;
	}

	@RequestMapping(method = RequestMethod.POST)
	LoanDto provideLoan(@RequestParam BigDecimal amount,
	                                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate returnDate,
	                                   HttpServletRequest request) {
		final String customersIp = ipAddressRetrievalService.getCustomersIp(request);
		final LoanDto loan = loanService.provideNewLoan(amount, returnDate.atStartOfDay(), customersIp);
		return loan;
	}

	@RequestMapping(method = RequestMethod.PUT)
	LoanDto prolongLoan(@RequestParam Integer id,
	                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate prolongationDate) {
		final LoanDto loan = loanService.prolongLoan(id, prolongationDate.atStartOfDay());
		return loan;
	}

	@ExceptionHandler(ProlongationDateException.class)
	ResponseEntity handleDateException() {
		return ResponseEntity.badRequest().body("Prolongation date must be later than current return date");
	}

	@ExceptionHandler(EntityNotFoundException.class)
	ResponseEntity handleEntityException() {
		return ResponseEntity.badRequest().body("Can't prolong non-existent loan");
	}
}
