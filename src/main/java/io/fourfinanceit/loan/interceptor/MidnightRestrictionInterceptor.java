package io.fourfinanceit.loan.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

@Component
public class MidnightRestrictionInterceptor implements HandlerInterceptor {

	@Value("${io.4f.restricted-hours-start}")
	@DateTimeFormat(pattern = "HH:mm")
	private LocalTime restrictedHoursStart;
	@Value("${io.4f.restricted-hours-end}")
	@DateTimeFormat(pattern = "HH:mm")
	private LocalTime restrictedHoursEnd;
	@Autowired
	private InterceptorResponseWriter interceptorResponseWriter;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		final LocalTime now = LocalTime.now();
		final boolean allowedToLoan = !(now.isAfter(restrictedHoursStart) && now.isBefore(restrictedHoursEnd));
		interceptorResponseWriter.write(allowedToLoan, response, "You're drunk, don't do that!");
		return allowedToLoan;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {}
}
