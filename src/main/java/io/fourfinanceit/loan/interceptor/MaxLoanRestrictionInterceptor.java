package io.fourfinanceit.loan.interceptor;

import io.fourfinanceit.geolocation.IPAddressRetrievalService;
import io.fourfinanceit.loan.domain.LoanRepository;
import io.fourfinanceit.loan.domain.Loan_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
public class MaxLoanRestrictionInterceptor implements HandlerInterceptor {

	@Value("${io.4f.max-loan-count}")
	private long maxLoanCountPerDay;
	@Autowired
	private IPAddressRetrievalService ipAddressRetrievalService;
	@Autowired
	private LoanRepository loanRepository;
	@Autowired
	private InterceptorResponseWriter interceptorResponseWriter;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if (isProlongation(request)) {
			return true;
		}
		final String customersIp = ipAddressRetrievalService.getCustomersIp(request);
		final long count = loanRepository.count((root, query, cb) -> cb.and(
				cb.equal(root.get(Loan_.ip), customersIp),
				cb.between(root.get(Loan_.loanStartDate), LocalDate.now().atStartOfDay(), LocalDateTime.now())
				));

		final boolean allowedToLoan = count < maxLoanCountPerDay;
		interceptorResponseWriter.write(allowedToLoan, response, "Exceeded maximum loan count!");

		return allowedToLoan;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {}

	private boolean isProlongation(HttpServletRequest request) {
		return RequestMethod.PUT.toString().equals(request.getMethod());
	}
}
