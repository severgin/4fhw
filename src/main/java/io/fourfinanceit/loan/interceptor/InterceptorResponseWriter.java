package io.fourfinanceit.loan.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
class InterceptorResponseWriter {

	private final ObjectMapper objectMapper = new ObjectMapper();

	void write(boolean allowedToLoan, HttpServletResponse response, String message) throws IOException {
		if (!allowedToLoan) {
			response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
			response.setStatus(HttpStatus.FORBIDDEN.value());
			response.getWriter().write(objectMapper.writeValueAsString(new Response(message)));
		}
	}
}
