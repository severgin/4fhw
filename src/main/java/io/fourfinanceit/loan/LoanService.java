package io.fourfinanceit.loan;

import io.fourfinanceit.loan.domain.Loan;
import io.fourfinanceit.loan.domain.LoanRepository;
import io.fourfinanceit.loan.domain.Prolongation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
class LoanService {

	private static final int DAYS_IN_WEEK = 7;

	private final LoanRepository loanRepository;

	private final BigDecimal interestRate;
	private final BigDecimal prolongationInterestRate;

	@Autowired
	LoanService(LoanRepository loanRepository,
	            @Value("${io.4f.interest-rate}") BigDecimal interestRate,
	            @Value("${io.4f.prolongation-interest-rate}") BigDecimal prolongationInterestRate) {
		this.loanRepository = loanRepository;
		this.interestRate = interestRate;
		this.prolongationInterestRate = prolongationInterestRate;
	}

	LoanDto provideNewLoan(BigDecimal amount, LocalDateTime returnDate, String ip) {
		final Loan loan = new Loan(amount.multiply(interestRate), ip, LocalDateTime.now(), returnDate);
		final Loan savedLoan = loanRepository.save(loan);
		return new LoanDto(savedLoan.getId(), savedLoan.getAmount(), savedLoan.getLoanEndDate());
	}

	@Transactional
	LoanDto prolongLoan(Integer id, LocalDateTime prolongationDate) {
		final Loan loan = loanRepository.getOne(id);

		final Optional<Prolongation> lastProlongation = loan.getProlongations().stream()
				.sorted((f1, f2) -> f2.getProlongedReturnDate().compareTo(f1.getProlongedReturnDate()))
				.findFirst();

		final BigDecimal prolongationWeeks = countProlongationWeeks(prolongationDate, lastProlongation, loan.getLoanEndDate());
		final BigDecimal prolongedAmount = lastProlongation.map(Prolongation::getProlongedAmount).orElse(loan.getAmount());

		final Prolongation prolongation = new Prolongation();
		prolongation.setProlongedAmount(prolongedAmount.multiply(prolongationInterestRate).multiply(prolongationWeeks));
		prolongation.setProlongedReturnDate(prolongationDate);
		loan.getProlongations().add(prolongation);
		loanRepository.save(loan);
		return new LoanDto(loan.getId(), prolongation.getProlongedAmount(), prolongation.getProlongedReturnDate());
	}

	BigDecimal countProlongationWeeks(LocalDateTime prolongationDate, Optional<Prolongation> lastProlongation, LocalDateTime loanEndDate) {
		final LocalDateTime storedLoanReturnDate = lastProlongation.map(Prolongation::getProlongedReturnDate).orElse(loanEndDate);
		if (storedLoanReturnDate.compareTo(prolongationDate) >= 0) {
			throw new ProlongationDateException("Prolongation date can't be earlier or equal to current return date!");
		}

		double days = ChronoUnit.DAYS.between(storedLoanReturnDate, prolongationDate);
		return BigDecimal.valueOf(days).divide(BigDecimal.valueOf(DAYS_IN_WEEK), 0, BigDecimal.ROUND_UP);
	}
}
