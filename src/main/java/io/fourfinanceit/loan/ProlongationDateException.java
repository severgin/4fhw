package io.fourfinanceit.loan;

class ProlongationDateException extends RuntimeException {

	ProlongationDateException(String message) {
		super(message);
	}
}
