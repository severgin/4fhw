package io.fourfinanceit.loan;

import io.fourfinanceit.geolocation.IPAddressRetrievalService;
import io.fourfinanceit.loan.domain.Loan;
import io.fourfinanceit.loan.domain.LoanRepository;
import io.fourfinanceit.loan.domain.Loan_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(path = "/loans", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
class LoansResource {

	private final LoanRepository loanRepository;
	private final IPAddressRetrievalService ipAddressRetrievalService;

	@Autowired
	LoansResource(LoanRepository loanRepository, IPAddressRetrievalService ipAddressRetrievalService) {
		this.loanRepository = loanRepository;
		this.ipAddressRetrievalService = ipAddressRetrievalService;
	}

	@RequestMapping("/all")
	List<Loan> allLoans() {
		return loanRepository.findAll();
	}

	@RequestMapping("/my")
	List<Loan> myLoans(HttpServletRequest request) {
		final String customersIp = ipAddressRetrievalService.getCustomersIp(request);
		return loanRepository.findAll((root, query, cb) -> cb.equal(root.get(Loan_.ip), customersIp));
	}
}
