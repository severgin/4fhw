package io.fourfinanceit.loan.domain;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@NoArgsConstructor
@ToString
@EqualsAndHashCode(exclude = "prolongations")
public class Loan {

	@Getter
	@Setter
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Integer id;

	@Getter
	@Setter
	private String ip;

	@Getter
	@Setter
	private BigDecimal amount;

	@Getter
	@Setter
	private LocalDateTime loanStartDate;

	@Getter
	@Setter
	private LocalDateTime loanEndDate;

	@Getter
	@Setter
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "loan_id", nullable = false, referencedColumnName = "id")
	private List<Prolongation> prolongations = new ArrayList<>();

	public Loan(BigDecimal amount, String ip, LocalDateTime loanStartDate, LocalDateTime loanEndDate) {
		this.amount = amount;
		this.ip = ip;
		this.loanStartDate = loanStartDate;
		this.loanEndDate = loanEndDate;
	}

}
