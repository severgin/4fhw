package io.fourfinanceit.loan.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@ToString
@EqualsAndHashCode
public class Prolongation {

	@Getter
	@Setter
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Integer id;

	@Getter
	@Setter
	private BigDecimal prolongedAmount;

	@Getter
	@Setter
	private LocalDateTime prolongedReturnDate;

}
