package io.fourfinanceit.geolocation;

import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class IPAddressRetrievalService {

	public String getCustomersIp(HttpServletRequest request) {
		return request.getRemoteAddr(); // call external ip service or if it down just select as is
	}

}
