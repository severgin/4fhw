package io.fourfinanceit;

import io.fourfinanceit.loan.interceptor.MaxLoanRestrictionInterceptor;
import io.fourfinanceit.loan.interceptor.MidnightRestrictionInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class AppWebConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addInterceptors(InterceptorRegistry registry){
		registry.addInterceptor(getMidnightRestrictionInterceptor()).addPathPatterns("/loan");
		registry.addInterceptor(getMaxLoanRestrictionInterceptor()).addPathPatterns("/loan");
	}

	@Bean
	public ConversionService conversionService() {
		return new DefaultFormattingConversionService();
	}

	@Bean
	MidnightRestrictionInterceptor getMidnightRestrictionInterceptor() {
		return new MidnightRestrictionInterceptor();
	}

	@Bean
	MaxLoanRestrictionInterceptor getMaxLoanRestrictionInterceptor() {
		return new MaxLoanRestrictionInterceptor();
	}

}
