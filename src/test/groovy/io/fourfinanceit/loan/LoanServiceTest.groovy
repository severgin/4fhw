package io.fourfinanceit.loan

import io.fourfinanceit.loan.domain.Loan
import io.fourfinanceit.loan.domain.LoanRepository
import io.fourfinanceit.loan.domain.Prolongation
import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime

class LoanServiceTest extends Specification {

    static final LocalDateTime TODAY_DATE = LocalDate.parse("2017-04-18").atStartOfDay()
    def loanRepository = Mock(LoanRepository)
    def loanService = new LoanService(loanRepository, 1.1, 1.5)

    def "should save new loan"() {
        setup:
        loanRepository.save(!null) >> new Loan(id:1, amount:110, loanStartDate: TODAY_DATE, loanEndDate: TODAY_DATE)

        when:
        def loanDto = loanService.provideNewLoan(100, TODAY_DATE, "127.0.0.1")

        then:
        loanDto.getId() == 1 && loanDto.getAmount() == 110
    }

    def "regular prolongation of loan"() {
        setup:
        def id = 1
        def loan = new Loan(id: id, ip: "127.0.0.1", amount: 100,
                loanStartDate: TODAY_DATE.minusDays(10), loanEndDate: TODAY_DATE.minusDays(5), prolongations: [])
        loanRepository.getOne(id) >> loan
        def prolongDate = TODAY_DATE

        when:
        def loanDto = loanService.prolongLoan(id, prolongDate)

        then:
        1 * loanRepository.save(loan)
        loanDto.getId() == id
        loanDto.getAmount() == 150
        loanDto.getReturnDate() == prolongDate
    }

    def "prolongation of already prolonged loan"() {
        setup:
        def id = 1
        def loan = new Loan(id: id, ip: "127.0.0.1", amount: 100,
                loanStartDate: TODAY_DATE.minusDays(20), loanEndDate: TODAY_DATE.minusDays(10),
                prolongations: [new Prolongation(id:1, prolongedAmount: 150, prolongedReturnDate: TODAY_DATE.minusDays(5))])
        loanRepository.getOne(id) >> loan
        def prolongDate = TODAY_DATE

        when:
        def loanDto = loanService.prolongLoan(id, prolongDate)

        then:
        1 * loanRepository.save(loan)
        loanDto.getId() == id
        loanDto.getAmount() == 225
        loanDto.getReturnDate() == prolongDate
    }

    def "provided date later than stored, everything ok"() {
        setup:
        def prolongationDate = TODAY_DATE
        def prolongation = Optional.of(new Prolongation(prolongedReturnDate: TODAY_DATE.minusDays(10)))
        def loanReturnDate = TODAY_DATE.minusDays(15)

        when:
        def weeks = loanService.countProlongationWeeks(prolongationDate, prolongation, loanReturnDate)

        then:
        weeks == 2
    }

    def "provided date same as stored, exception"() {
        setup:
        def prolongationDate = TODAY_DATE
        def prolongation = Optional.of(new Prolongation(prolongedReturnDate: TODAY_DATE))
        def loanReturnDate = TODAY_DATE.minusDays(15)

        when:
        loanService.countProlongationWeeks(prolongationDate, prolongation, loanReturnDate)

        then:
        ProlongationDateException ex = thrown()
        ex.getMessage() == "Prolongation date can't be earlier or equal to current return date!"
    }
}
