package io.fourfinanceit.loan.interceptor

import io.fourfinanceit.geolocation.IPAddressRetrievalService
import io.fourfinanceit.loan.domain.LoanRepository
import org.springframework.web.bind.annotation.RequestMethod
import spock.lang.Specification

import javax.servlet.http.HttpServletRequest

class MaxLoanRestrictionInterceptorTest extends Specification {

    def maxLoanCountPerDay = 2

    def mockIPAddressRetrievalService = Mock(IPAddressRetrievalService)
    def loanRepository = Mock(LoanRepository)
    def interceptorResponseWriter = Mock(InterceptorResponseWriter)
    def request = Mock(HttpServletRequest)
    def maxLoanRestrictionInterceptor = new MaxLoanRestrictionInterceptor(maxLoanCountPerDay: maxLoanCountPerDay,
            ipAddressRetrievalService:mockIPAddressRetrievalService, loanRepository:loanRepository, interceptorResponseWriter:interceptorResponseWriter)

    def "provide new loans for user"() {
        loanRepository.count(!null) >> loanCount
        request.getMethod() >> RequestMethod.POST.toString()

        expect: "forbid user to loan more than ${maxLoanCountPerDay} times"
        allowedToLoan == maxLoanRestrictionInterceptor.preHandle(request, null, null)

        where:
        loanCount   | allowedToLoan
        0           | true
        1           | true
        2           | false

    }

    def "ignore prolongation"() {
        loanRepository.count(!null) >> loanCount
        request.getMethod() >> RequestMethod.PUT.toString()

        expect: "ignore restriction for prolongation"
        allowedToLoan == maxLoanRestrictionInterceptor.preHandle(request, null, null)

        where:
        loanCount   | allowedToLoan
        0           | true
        1           | true
        2           | true

    }

}
