package io.fourfinanceit.loan.interceptor

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import spock.lang.Specification

import javax.servlet.http.HttpServletResponse

class InterceptorResponseWriterTest extends Specification {

    def response = Mock(HttpServletResponse)
    def writer = Mock(PrintWriter)
    def interceptorResponseWriter = new InterceptorResponseWriter()
    def objectMapper = new ObjectMapper()

    def "Nothing written in output if no restriction found"() {
        when:
        interceptorResponseWriter.write(true, response, "Error message")

        then:
        0 * response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        0 * response.setStatus(HttpStatus.FORBIDDEN.value())
        0 * response.getWriter().write(objectMapper.writeValueAsString(new Response("Error message")))
    }

    def "Error is written in output if restriction found"() {
        given:
        response.getWriter() >> writer

        when:
        interceptorResponseWriter.write(false, response, "Error message")

        then:
        1 * response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        1 * response.setStatus(HttpStatus.FORBIDDEN.value())
        1 * writer.write(objectMapper.writeValueAsString(new Response("Error message")))
    }

}
