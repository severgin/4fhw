package io.fourfinanceit.loan.interceptor

import spock.lang.Specification

import java.time.LocalTime

class MidnightRestrictionInterceptorTest extends Specification {

    public static final LocalTime REQUEST_TIME = LocalTime.now()
    def interceptorResponseWriter = Mock(InterceptorResponseWriter)

    def "validate allowed hours for loan"() {

        expect: "forbid user to loan in drunk hours"
        def startTime = REQUEST_TIME.plusHours(startTimeShift).minusMinutes(1)
        def endTime = REQUEST_TIME.plusHours(endTimeShift)
        allowedToLoan == new MidnightRestrictionInterceptor(restrictedHoursStart: startTime, restrictedHoursEnd:endTime,
                interceptorResponseWriter:interceptorResponseWriter).preHandle(null, null, null)

        where:
        startTimeShift  | endTimeShift  | allowedToLoan
        -1              | 1             | false
        0               | 1             | false
        0               | 0             | true
        -2              | -1            | true

    }
}
