package io.fourfinanceit.loan

import groovy.json.JsonSlurper
import io.fourfinanceit.geolocation.IPAddressRetrievalService
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import javax.persistence.EntityNotFoundException
import java.time.LocalDate
import java.time.LocalDateTime

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

class LoanResourceSpecification extends Specification {

    def mockIPAddressRetrievalService = Spy(IPAddressRetrievalService)
    def mockLoanService = Mock(LoanService)
    def loanResource = new LoanResource(mockLoanService, mockIPAddressRetrievalService)

    static final String TODAY = "2017-04-17"
    public static final LocalDateTime TODAY_DATE = LocalDate.parse(TODAY).atStartOfDay()

    MockMvc mockMvc = standaloneSetup(loanResource).build()
    def jsonSlurper = new JsonSlurper()

    def "request for loan"() {
        given: "services for loan save"
        mockLoanService.provideNewLoan(12.3, TODAY_DATE, "127.0.0.1") >> new LoanDto(1, 12.3, TODAY_DATE)
        mockIPAddressRetrievalService.getCustomersIp(!null) >> "127.0.0.1"

        when: "consumer call new loan request service"
        def requestBuilder = post('/loan')
        requestBuilder.param("amount", "12.3")
        requestBuilder.param("returnDate", TODAY)
        def response = mockMvc.perform(requestBuilder).andReturn().response

        then: "application returns json containing provided loan"

        def resultJson = jsonSlurper.parse(response.contentAsString.toCharArray())
        resultJson.id == 1
    }

    def "prolong loan"() {
        given: "services for loan prolongation"
        mockLoanService.prolongLoan(1, TODAY_DATE) >> new LoanDto(1, 12.3, TODAY_DATE)

        when: "consumer call prolong loan request service"
        def requestBuilder = put('/loan')
        requestBuilder.param("id", "1")
        requestBuilder.param("prolongationDate", TODAY)
        def response = mockMvc.perform(requestBuilder).andReturn().response

        then: "application returns json containing prolonged loan"

        def resultJson = jsonSlurper.parse(response.contentAsString.toCharArray())
        resultJson.id == 1
    }

    def "try prolong loan with the same date, should be exception"() {
        given: "services for loan prolongation"
        mockLoanService.prolongLoan(1, TODAY_DATE) >> { throw new ProlongationDateException("Oops!")}

        when: "consumer call prolong loan request service"
        def requestBuilder = put('/loan')
        requestBuilder.param("id", "1")
        requestBuilder.param("prolongationDate", TODAY)
        def response = mockMvc.perform(requestBuilder).andReturn().response

        then: "application returns error message"

        "Prolongation date must be later than current return date" == response.contentAsString
    }

    def "try prolong non-existent loan, should be exception"() {
        given: "services for loan prolongation"
        mockLoanService.prolongLoan(1, TODAY_DATE) >> { throw new EntityNotFoundException("Oops!")}

        when: "consumer call prolong loan request service"
        def requestBuilder = put('/loan')
        requestBuilder.param("id", "1")
        requestBuilder.param("prolongationDate", TODAY)
        def response = mockMvc.perform(requestBuilder).andReturn().response

        then: "application returns error message"

        "Can't prolong non-existent loan" == response.contentAsString
    }

}
