package io.fourfinanceit.loan

import groovy.json.JsonSlurper
import io.fourfinanceit.geolocation.IPAddressRetrievalService
import io.fourfinanceit.loan.domain.Loan
import io.fourfinanceit.loan.domain.LoanRepository
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.http.HttpStatus.OK

class LoansResourceSpecification extends Specification {

    static final LocalDateTime TODAY_DATE = LocalDate.parse("2017-04-18").atStartOfDay()
    def mockLoanRepository = Mock(LoanRepository)
    def mockIPAddressRetrievalService = Spy(IPAddressRetrievalService)
    def loansResource = new LoansResource(mockLoanRepository, mockIPAddressRetrievalService)

    MockMvc mockMvc = standaloneSetup(loansResource).build()
    def jsonSlurper = new JsonSlurper()

    def loans = [
            new Loan(id: 1, ip: "127.0.0.1", amount: 13.3, loanStartDate: TODAY_DATE, loanEndDate: TODAY_DATE.plusWeeks(2)),
            new Loan(id: 2, ip: "192.168.0.1", amount: 75.56, loanStartDate: TODAY_DATE, loanEndDate: TODAY_DATE.plusWeeks(1))
    ]

    def "load all loans"() {
        given: "database containing list of loans"
        mockLoanRepository.findAll() >> loans

        when: "consumer requests all stored loans"
        def response = mockMvc.perform(get('/loans/all')).andReturn().response

        then: "application returns json containing stored loans"

        validateReturnState(response)
        def resultList = jsonSlurper.parse(response.contentAsString.toCharArray())

        loans.eachWithIndex { loan, index ->
            resultList[index].id == loan.id && resultList[index].amount == loan.amount
        }
    }

    def "load my loans"() {
        given: "database containing list of loans"
        mockLoanRepository.findAll(!null) >> loans.findAll {it.ip == "127.0.0.1"}
        mockIPAddressRetrievalService.getCustomersIp(!null) >> "127.0.0.1"

        when: "consumer requests all stored loans"
        def response = mockMvc.perform(get('/loans/my')).andReturn().response

        then: "application returns json containing stored loans"

        validateReturnState(response)
        def resultList = jsonSlurper.parse(response.contentAsString.toCharArray())

        resultList.size == 1
        loans[0].id == resultList[0].id && resultList[0].amount == loans[0].amount
    }

    private void validateReturnState(MockHttpServletResponse response) {
        response.status == OK.value()
        response.contentType.contains('application/json')
        response.contentType == 'application/json;charset=UTF-8'
    }
}
