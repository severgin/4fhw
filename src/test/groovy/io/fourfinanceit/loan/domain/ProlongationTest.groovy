package io.fourfinanceit.loan.domain

import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime

class ProlongationTest extends Specification {

    static final LocalDateTime TODAY_DATE = LocalDate.parse("2017-04-18").atStartOfDay()

    def "test equals and hashcode for Prolongation domain object"() {
        setup:
        def firstProlongation = new Prolongation()
        firstProlongation.setId(1)
        firstProlongation.setProlongedAmount(123)
        firstProlongation.setProlongedReturnDate(TODAY_DATE)

        expect:
        def secondProlongation = new Prolongation(id: id, prolongedAmount: prolongedAmount, prolongedReturnDate: prolongedReturnDate)
        prolongationsAreEqual == secondProlongation.equals(firstProlongation)
        prolongationsAreEqual == secondProlongation.hashCode().equals(firstProlongation.hashCode())

        where:
        id | prolongedAmount   | prolongedReturnDate       | prolongationsAreEqual
        1  | 123   			   | TODAY_DATE                | true
        2  | 123   			   | TODAY_DATE                | false
        1  | 433   			   | TODAY_DATE                | false
        1  | 123   			   | TODAY_DATE.plusDays(10)   | false
    }

    def "test accessors for Prolongation domain object"() {
        when:
        def firstProlongation = new Prolongation()
        firstProlongation.setId(1)
        firstProlongation.setProlongedAmount(123)
        firstProlongation.setProlongedReturnDate(TODAY_DATE)

        def secondProlongation = new Prolongation(id: 1,prolongedAmount: 123, prolongedReturnDate: TODAY_DATE)

        then:
        firstProlongation.getId() == secondProlongation.getId()
        firstProlongation.getProlongedAmount() == secondProlongation.getProlongedAmount()
        firstProlongation.getProlongedReturnDate() == secondProlongation.getProlongedReturnDate()
    }

}
