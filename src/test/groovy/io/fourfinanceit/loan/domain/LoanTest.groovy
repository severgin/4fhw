package io.fourfinanceit.loan.domain

import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime

class LoanTest extends Specification {


    public static final LocalDateTime TODAY_DATE = LocalDate.parse("2017-04-18").atStartOfDay()
    public static final String DEFAULT_IP = "127.0.0.1"

    def "test equals and hashcode for Loan domain object"() {
        setup:
        def now = TODAY_DATE

        def firstLoan = new Loan()
        firstLoan.setId(1)
        firstLoan.setIp(DEFAULT_IP)
        firstLoan.setAmount(123)
        firstLoan.setLoanEndDate(now)
        firstLoan.setLoanStartDate(now)

        expect:
        def secondLoan = new Loan(id: id, ip:ip, amount: amount, loanStartDate: startDate, loanEndDate: endDate)
        loansAreEqual == secondLoan.equals(firstLoan)
        loansAreEqual == secondLoan.hashCode().equals(firstLoan.hashCode())

        where:
        id	| ip          	| amount | startDate               | endDate                 | loansAreEqual
        1 	| DEFAULT_IP    | 123    | TODAY_DATE              | TODAY_DATE              | true
        2 	| DEFAULT_IP    | 123    | TODAY_DATE              | TODAY_DATE              | false
        1 	| "192.168.1.1" | 123    | TODAY_DATE              | TODAY_DATE              | false
        1 	| DEFAULT_IP    | 525    | TODAY_DATE              | TODAY_DATE              | false
        1 	| DEFAULT_IP    | 123    | TODAY_DATE.plusDays(10) | TODAY_DATE              | false
        1 	| DEFAULT_IP    | 123    | TODAY_DATE              | TODAY_DATE.plusDays(10) | false
    }

    def "test accessors for Loan domain object"() {
        when:
        def now = TODAY_DATE

        def firstLoan = new Loan()
        firstLoan.setId(1)
        firstLoan.setIp(DEFAULT_IP)
        firstLoan.setAmount(123)
        firstLoan.setLoanEndDate(now)
        firstLoan.setLoanStartDate(now)
        firstLoan.setProlongations([])

        def secondLoan = new Loan(id: 1, ip: DEFAULT_IP, amount: 123, loanStartDate: now, loanEndDate: now, prolongations: [])

        then:
        firstLoan.getId() == secondLoan.getId()
        firstLoan.getIp() == secondLoan.getIp()
        firstLoan.getAmount() == secondLoan.getAmount()
        firstLoan.getLoanEndDate() == secondLoan.getLoanEndDate()
        firstLoan.getLoanStartDate() == secondLoan.getLoanStartDate()
        firstLoan.getProlongations().size() == secondLoan.getProlongations().size()
    }
}
