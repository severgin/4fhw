package io.fourfinanceit.loan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LoanServiceIntegrationTest {

	private static final String IP = "127.0.0.1";
	private static final int NEW_LOAN_AMOUNT = 100;
	private static final int SCALE_TO_CENTS = 2;
	private static final LocalDateTime TODAY_DATE = LocalDate.parse("2017-04-18").atStartOfDay();

	@Autowired
	private LoanService loanService;

	@Test
	public void shouldSaveNewLoan() {
		final LocalDateTime returnDate = TODAY_DATE.plusDays(10);
		final LoanDto loanDto = loanService.provideNewLoan(BigDecimal.valueOf(NEW_LOAN_AMOUNT), returnDate, IP);
		assertEquals(BigDecimal.valueOf(110).setScale(SCALE_TO_CENTS), loanDto.getAmount());
		assertEquals(returnDate, loanDto.getReturnDate());
		assertNotNull(loanDto.getId());
	}

	@Test
	public void shouldSaveLoansWithNewIds() {
		final LocalDateTime returnDate = TODAY_DATE.plusDays(10);
		final LoanDto firstLoanDto = loanService.provideNewLoan(BigDecimal.valueOf(NEW_LOAN_AMOUNT), returnDate, IP);
		final LoanDto secondLoanDto = loanService.provideNewLoan(BigDecimal.valueOf(NEW_LOAN_AMOUNT), returnDate, IP);
		assertNotEquals(firstLoanDto.getId(), secondLoanDto.getId());
	}

	@Test
	@Transactional
	public void shouldProlongLoan() {
		final LocalDateTime returnDate = TODAY_DATE.plusDays(10);
		final LoanDto loanDto = loanService.provideNewLoan(BigDecimal.valueOf(NEW_LOAN_AMOUNT), returnDate, IP);

		final LoanDto prolongedLoanDto = loanService.prolongLoan(loanDto.getId(), returnDate.plusDays(5));
		assertEquals(loanDto.getId(), prolongedLoanDto.getId());
		assertEquals(BigDecimal.valueOf(165).setScale(SCALE_TO_CENTS), prolongedLoanDto.getAmount());

		final LoanDto secondProlongationDto = loanService.prolongLoan(loanDto.getId(), returnDate.plusDays(10));
		assertEquals(loanDto.getId(), secondProlongationDto.getId());
		assertEquals(BigDecimal.valueOf(247.5).setScale(SCALE_TO_CENTS), secondProlongationDto.getAmount());
	}
}