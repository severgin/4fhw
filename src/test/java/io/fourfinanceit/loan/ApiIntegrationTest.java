package io.fourfinanceit.loan;

import io.fourfinanceit.loan.domain.Loan;
import io.fourfinanceit.loan.domain.LoanRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;
	@Autowired
	private LoanRepository loanRepository;
	@Value("${io.4f.max-loan-count}")
	private long maxLoanCountPerDay;

	@Test
	public void loadLoanList() throws IOException {

		ResponseEntity<List<Loan>> responseEntity = restTemplate.exchange("/loans/all", HttpMethod.GET, null, new ParameterizedTypeReference<List<Loan>>() {});
		List<Loan> allLoans = responseEntity.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(3, allLoans.size());
	}

	@Test
	public void loadMyLoansList() throws IOException {
		final MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
		map.add("amount", "100");
		map.add("returnDate", "2017-04-18");

		final LoanDto loanDto = requestLoan("/loan", map, HttpMethod.POST, LoanDto.class).getBody();

		ResponseEntity<List<Loan>> responseEntity = restTemplate.exchange("/loans/my", HttpMethod.GET, null, new ParameterizedTypeReference<List<Loan>>() {});
		List<Loan> myLoans = responseEntity.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(1, myLoans.size());
		assertEquals(BigDecimal.valueOf(110).setScale(2), myLoans.get(0).getAmount().setScale(2));

		loanRepository.delete(loanDto.getId());
	}

	@Test
	public void provideLoan() throws IOException {
		final MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
		map.add("amount", "100");
		map.add("returnDate", "2017-04-18");

		ResponseEntity<LoanDto> response = requestLoan("/loan", map, HttpMethod.POST, LoanDto.class);
		final LoanDto providedLoan = response.getBody();

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(BigDecimal.valueOf(110).setScale(2), providedLoan.getAmount().setScale(2));
		assertEquals(LocalDate.parse("2017-04-18").atStartOfDay(), providedLoan.getReturnDate());

		loanRepository.delete(providedLoan.getId());
	}

	@Test
	public void prolongLoan() throws IOException {
		final MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
		map.add("amount", "100");
		map.add("returnDate", "2017-04-18");

		final LoanDto loanDto = requestLoan("/loan", map, HttpMethod.POST, LoanDto.class).getBody();

		map.clear();
		map.add("id", loanDto.getId().toString());
		map.add("prolongationDate", "2017-04-22");

		final ResponseEntity<LoanDto> requestResult = requestLoan("/loan", map, HttpMethod.PUT, LoanDto.class);
		final LoanDto prolongedLoan = requestResult.getBody();

		assertEquals(HttpStatus.OK, requestResult.getStatusCode());
		assertEquals(BigDecimal.valueOf(165).setScale(2), prolongedLoan.getAmount().setScale(2));
		assertEquals(LocalDate.parse("2017-04-22").atStartOfDay(), prolongedLoan.getReturnDate());

		loanRepository.delete(prolongedLoan.getId());
	}

	@Test
	public void prolongLoanWithTheSameDate() throws IOException {
		final MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
		map.add("amount", "100");
		map.add("returnDate", "2017-04-18");

		final LoanDto loanDto = requestLoan("/loan", map, HttpMethod.POST, LoanDto.class).getBody();

		map.clear();
		map.add("id", loanDto.getId().toString());
		map.add("prolongationDate", "2017-04-18");

		final ResponseEntity<String> requestResult = requestLoan("/loan", map, HttpMethod.PUT, String.class);
		final String errorMessage = requestResult.getBody();

		assertEquals(HttpStatus.BAD_REQUEST, requestResult.getStatusCode());
		assertEquals("Prolongation date must be later than current return date", errorMessage);

		loanRepository.delete(loanDto.getId());
	}

	@Test
	public void prolongNotExistingLoan() throws IOException {
		final MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
		map.add("id", "100");
		map.add("prolongationDate", "2017-04-18");

		final ResponseEntity<String> requestResult = requestLoan("/loan", map, HttpMethod.PUT, String.class);
		final String errorMessage = requestResult.getBody();

		assertEquals(HttpStatus.BAD_REQUEST, requestResult.getStatusCode());
		assertEquals("Can't prolong non-existent loan", errorMessage);
	}

	@Test
	public void requestLoanExceedingMaxAttemptsPerDay() throws IOException {
		final MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
		map.add("amount", "100");
		map.add("returnDate", "2017-04-18");

		List<Integer> ids = new ArrayList<>();
		for(int i = 0; i < maxLoanCountPerDay; i++) {
			final LoanDto loanDto = requestLoan("/loan", map, HttpMethod.POST, LoanDto.class).getBody();
			ids.add(loanDto.getId());
		}

		final ResponseEntity<String> requestResult = requestLoan("/loan", map, HttpMethod.POST, String.class);
		final String errorMessage = requestResult.getBody();

		assertEquals("{\"responseMessage\":\"Exceeded maximum loan count!\"}", errorMessage);

		ids.stream().forEach(item -> loanRepository.delete(item));
	}

	private <T> ResponseEntity<T> requestLoan(String url, MultiValueMap<String, String> requestParameters, HttpMethod httpMethod, Class<T> returnType) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(requestParameters, headers);

		ResponseEntity<T> response = restTemplate.exchange(url, httpMethod, request, returnType);
		return response;
	}

}
