package io.fourfinanceit;

import io.fourfinanceit.loan.domain.LoanRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.spockframework.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HomeworkApplicationTests {

	@Autowired
	private LoanRepository loanRepository;

	@Test
	public void contextLoads() {
		Assert.notNull(loanRepository);
	}

}
