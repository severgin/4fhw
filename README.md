# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Homework for Java Software Developer position
* ver. 17.4.0
* Repository url: https://bitbucket.org/severgin/4fhw

### How do I get set up? ###

* Prerequisites: Java 8, Gradle, Git
* to run app just type in terminal: gradle bootRun
* db console available by http://localhost:8080/h2-console dbname: testdb, user: sa

### Who do I talk to? ###

* Alexander Severgin